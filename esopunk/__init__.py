from . import exa
from . import instrs

def parse(f):
	code = []
	with f:
		for i, line in enumerate(f):
			line = line.upper()
			line = line.rstrip()
			if(line):
				try:
					line = line[:line.index(";")]
				except ValueError:
					pass
				
				split = line.split()
				if(split):
					instruction = instrs.get(split, lineno=i)
					#print(instruction)
					code.append(instruction)
					continue
				
			
	return code

def run(f):
	code = parse(f)
	exas = [exa.EXA(code)]
	
	while(len(exas) > 0):
		for i, xa in enumerate(exas):
			xa()
			#print(xa)
			if(not xa.alive):
				exas.pop(i)
