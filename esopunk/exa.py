import sys
from enum import Enum

from . import errors

class Mode(Enum):
	GLOBAL = 0
	LOCAL = 1

class EXA:
	def __init__(self, code, ip=0):
		self.code = code
		self.ip = ip
		self.alive = True
		
		self.X = 0
		self.T = 0
		self.M = None
		
		self.mode = Mode.GLOBAL
	
	def switchmode(self):
		if(self.mode == Mode.GLOBAL):
			self.mode == Mode.LOCAL
		else:
			self.mode == Mode.GLOBAL
	
	def halt(self):
		self.running = False
	
	def jump(self, label):
		for i, line in enumerate(self.code):
			if(hasattr(line, "label")):
				if(line.label == label):
					self.ip = i
					#print(f"[Jumping to {label}. IP={self.ip} {self.code[self.ip]}]")
					return
		raise Exception(f"Label {label} not found.")
	
	def __call__(self):
		#print(self)
		if(self.ip >= len(self.code)):
			self.alive = False
			return
		
		self.code[self.ip](self)
		self.ip += 1
	
	def __getitem__(self, value):
		try:
			return int(value)
		except:
			if(value == "X"):
				return self.X
			if(value == "T"):
				return self.T
			if(value == "M"):
				raise ArgumentError("TODO: M register")
			if(value == "#STDI"):
				read = sys.stdin.read(1)
				if(read):
					return ord(read[0])
				else:
					return -1
		
		raise Exception(value)
	
	def __setitem__(self, name, value):
		if(name == "X"):
			self.X = self[value]
		if(name == "T"):
			self.T = self[value]
		if(name == "M"):
			raise ArgumentError("TODO: M register")
		if(name == "#STDO"):
			c = chr(value)
			sys.stdout.write(c)
		if(name == "#STDE"):
			c = chr(value)
			sys.stdout.write(c)
	
	def __repr__(self):
		return f"<EXA(X={self.X},T={self.T})>"
