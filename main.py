#!/usr/bin/env python3
import sys
import argparse

import esopunk


parser = argparse.ArgumentParser(description="pyESOPUNK -- Python ESOPUNK")

parser.add_argument("file",
	metavar="FILE",
	nargs="?",
	type=argparse.FileType('r'),
	help="File to run."
)

parser.add_argument("-v", 
	action="count",
	default=0,
	help="Verbose output."
)

parser.add_argument("-s",
	metavar="STRING",
	type=str,
	help="Generate a string constant"
)

ns = parser.parse_args()

#print(ns)

if(ns.s):
	# Generate a string constant
	# TODO: There's probably better strategies than this
	if(ns.v > 0):
		fmt = "COPY {n:03d} #STDO ; '{c}'"
	else:
		fmt = "COPY {n} #STDO"
	
	for c in ns.s:
		print(fmt.format(c=c, n=ord(c)))
	exit(0)


esopunk.run(ns.file)
